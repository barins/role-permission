<?php

return [
    'log' => [

        /*
         * Record Logs.
         */

        'record' => true,

        /*
         * Display the user in the logs which is performing action.
         */

        'show_user' => true,

        /*
         * The name of the channel where the logs will be recorded.
         */

        'channel' => 'permission'
    ],

    /*
     * The name of the table.
     */

    'table_names' => [
        'roles' => 'roles',
        'user_has_roles' => 'user_has_roles'
    ],

    'models' => [

        /*
         * Namespace model of role.
         */

        'role' => BorisNedovis\RolePermission\Models\Role::class,
    ],

    'redis' => [

        /*
         * The cache key used to store all permissions.
         */

        'key' => 'permission',


        /*
         * Prefix fot keys of redis.
         */

        'prefix' => 'laravel_database_'
    ],

    /*
     * Redirect to pages if the user does not have access for middleware
     */

    'middleware_redirect' => '/',

    /*
     * All permission
     */

    'permissions' => [
        ///
    ]
];