# Package for checking permission

### Install:

- In composer.json add this package
```php
"require": {
        ...
        "boris-nedovis/role-permission": "dev-master"
},
"repositories": [
        {
            "type":"vcs",
            "url":"ssh://git@95.211.204.61:8415/eid/rolepermission.git",
        }
],
"autoload": {
        "psr-4": {
            ...
            "BorisNedovis\\RolePermission\\": "vendor/boris-nedovis/role-permission/src/"
        }
}
```
- composer update
- Add provider in 'App\config\app.php' in key 'providers'
```php
// Adding provider
BorisNedovis\RolePermission\Provider\PermissionServiceProvider::class
```
- Add middleware in App\Http\Kernel.php
```php
protected $routeMiddleware = [
        ...
        'permission' => \BorisNedovis\RolePermission\Middleware\PermissionMiddleware::class,
];
```
- php artisan vendor:publish and select this provider
- php artisan migrate
- php artisan config:cache as well as customize them
- Add traits to your model
```php
// In namespace
use BorisNedovis\RolePermission\Traits\Role;
use BorisNedovis\RolePermission\Traits\Permissions;

  use Role;
  use Permissions;
```
## Using:
```php
// Create role
$role = Role::create(['name'=> 'Manager', 'permission' => array('dashboard', 'messages')])

// Add this role to user
$user::addRole($role)

// Delete this to role
$user::deleteRole($role)

// Permission check($permission = string)
$user::checkPermission($permission): bool

// You can also use the method "can()" which comes from the box Laravel
$user::can($permission): bool

// For route
Route::get('logs', 'LogsController@index')->middleware('permission:logs|statistics');

```

## Artisan commands:

- php artisan permission:cache-reset 
- php artisan permission:show {model - optional} {user_id - optional}

Happy coding!