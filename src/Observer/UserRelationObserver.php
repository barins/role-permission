<?php

namespace BorisNedovis\RolePermission\Observers;

use BorisNedovis\RolePermission\Traits\LogData;
use BorisNedovis\RolePermission\Models\UserHasRole;

/**
 * Class RoleObserver
 *
 * @package App\Observers
 *
 * @property bool   $logRecord   required
 * @property string $logChannel  required
 * @property string $logShowAuth required
 */
class UserRelationObserver
{
    use LogData;

    public $logRecord;
    public $logChannel;
    public $logShowAuth;

    public function __construct()
    {
        $this->logRecord = config('role-permission.log.record');
        $this->logChannel = config('role-permission.log.channel');
        $this->logShowAuth = config('role-permission.log.show_user');
    }

    /**
     * Listen to the Role created event.
     *
     * @param UserHasRole $relation
     * @return void
     */
    public function creating(UserHasRole $relation)
    {
      $this->log("Create relation: $relation");
    }

    /**
     * Listen to the Role deleting event.
     *
     * @param UserHasRole $relation
     * @return void
     */
    public function deleting(UserHasRole $relation)
    {
        $this->log("Delete relation: $relation");
    }

}