<?php

namespace BorisNedovis\RolePermission\Observers;

use BorisNedovis\RolePermission\Models\Role;
use BorisNedovis\RolePermission\Traits\LogData;
use BorisNedovis\RolePermission\PermissionRegistrar;

/**
 * Class RoleObserver
 *
 * @package App\Observers
 *
 * @property bool   $logRecord   required
 * @property string $logChannel  required
 * @property string $logShowAuth required
 */
class RoleObserver
{
    use LogData;

    public $logRecord;
    public $logChannel;
    public $logShowAuth;

    public function __construct()
    {
        $this->logRecord = config('role-permission.log.record');
        $this->logChannel = config('role-permission.log.channel');
        $this->logShowAuth = config('role-permission.log.show_user');
    }

    /**
     * Listen to the Role created event.
     *
     * @param Role $role
     * @return void
     */
    public function creating(Role $role)
    {
      $role['permission'] = app(PermissionRegistrar::class)->preparePermissions($role['permission'], true);

      $this->log($role);
    }

    /**
     * Listen to the Role updating event.
     *
     * @param Role $role
     * @return void
     */
    public function updating(Role $role)
    {
        $role['permission'] = app(PermissionRegistrar::class)->preparePermissions($role['permission'], true);

        $old_role = Role::whereId($role['id'])->first();

        $this->log($this->prepareLogForUpdatingRole($old_role, $role));
    }

    /**
     * Listen to the Role deleting event.
     *
     * @param Role $role
     * @return void
     */
    public function deleting(Role $role)
    {
        $this->log("Delete: $role");
    }

    /**
     * @param $old_role
     * @param $new_role
     *
     * @return string
     */
    protected function prepareLogForUpdatingRole($old_role, $new_role)
    {
        $new_role = json_encode($new_role);

        return
            "Edit: Old( $old_role ) | New( $new_role )";
    }
}