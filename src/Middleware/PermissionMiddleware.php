<?php

namespace BorisNedovis\RolePermission\Middleware;

use Closure;

class PermissionMiddleware
{
    public function handle($request, Closure $next, $permission)
    {
        $permissions = is_array($permission)
            ? $permission
            : explode('|', $permission);

        foreach ($permissions as $permission) {
            if (! app('auth')->user()->can($permission)) {
                return redirect(config('role-permission.middleware_redirect'));
            }
        }

        return $next($request);
    }
}