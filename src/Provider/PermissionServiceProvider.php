<?php

namespace BorisNedovis\RolePermission\Provider;

use Illuminate\Routing\Route;
use Illuminate\Support\Collection;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use BorisNedovis\RolePermission\Models\Role;
Use BorisNedovis\RolePermission\Commands\Show;
use BorisNedovis\RolePermission\Models\UserHasRole;
Use BorisNedovis\RolePermission\Commands\CacheReset;
use BorisNedovis\RolePermission\PermissionRegistrar;
use BorisNedovis\RolePermission\Observers\RoleObserver;
use BorisNedovis\RolePermission\Observers\UserRelationObserver;

/**
 * Class PermissionServiceProvider
 *
 * @package BorisNedovis\RolePermission\Provider
 */
class PermissionServiceProvider extends ServiceProvider
{
    /**
     * @param PermissionRegistrar $permissionLoader
     *
     * @param Filesystem $filesystem
     */
    public function boot(PermissionRegistrar $permissionLoader, Filesystem $filesystem)
    {
        $this->publishes([
            __DIR__.'/../../config/role-permission.php' => config_path('role-permission.php'),
        ], 'config');

        $this->publishes([
            __DIR__.'/../../database/migrations/create_permission_tables.php.stub' => $this->getMigrationFileName($filesystem),
        ], 'migrations');

        $this->registerMacroHelpers();

        $this->registerCommands();

        $this->registerObservers();

        $permissionLoader->registerPermissions();
    }

    protected function registerCommands()
    {
        $this->commands([
            CacheReset::class,
            Show::class,
        ]);
    }

    protected function registerMacroHelpers()
    {
        Route::macro('permission', function ($permissions = []) {
            if (! is_array($permissions)) {
                $permissions = [$permissions];
            }

            $permissions = implode('|', $permissions);

            $this->middleware("permission:$permissions");

            return $this;
        });
    }

    protected function registerObservers()
    {
        Role::observe(RoleObserver::class);
        UserHasRole::observe(UserRelationObserver::class);
    }

    /**
     * Returns existing migration file if found, else uses the current timestamp.
     *
     * @param Filesystem $filesystem
     * @return string
     */
    protected function getMigrationFileName(Filesystem $filesystem): string
    {
        $timestamp = date('Y_m_d_His');

        return Collection::make($this->app->databasePath().DIRECTORY_SEPARATOR.'migrations'.DIRECTORY_SEPARATOR)
            ->flatMap(function ($path) use ($filesystem) {
                return $filesystem->glob($path.'*_create_permission_tables.php');
            })->push($this->app->databasePath()."/migrations/{$timestamp}_create_permission_tables.php")
            ->first();
    }

}