<?php

namespace BorisNedovis\RolePermission\Exceptions;

use InvalidArgumentException;

class RoleAlreadyExists extends InvalidArgumentException
{
    public static function create(string $roleName, string $guardName)
    {
        return new static("A role `{$roleName}` already exists for guard `{$guardName}`.");
    }

    public static function createRelation(string $model, int $roleId, int $userId)
    {
        return new static("Role `{$roleId}` is already in this user `{$model} id:{$userId}`.");

    }
}
