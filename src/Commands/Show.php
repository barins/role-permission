<?php

namespace BorisNedovis\RolePermission\Commands;

use Illuminate\Console\Command;
use BorisNedovis\RolePermission\PermissionRegistrar;
use BorisNedovis\RolePermission\Models\UserHasRole;

class Show extends Command
{
    /**
     * User title field name.
     *
     * @var string
     */
    protected $fieldName;

    /** @var string */
    protected $styleTable;

    /** @var \phpDocumentor\Reflection\Types\Collection */
    protected $userHasRole;

    protected $signature = 'permission:show
            {model?   : The name of the model}
            {user_id? : The id user in selected model}';

    protected $description = 'Show permissions';

    /**
     * Show constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->styleTable = 'box';

        /** Builder */
        $this->userHasRole = UserHasRole::query();

        $this->fieldName = 'name';
    }

    /**
     * Handle command
     */
    public function handle()
    {
        $this->getModelsWithActiveRoles();

        if(is_null($this->argument('model'))) {
            $this->getAllPermissions();
            exit;
        }

        $this->info($this->argument('model').':');

        if(is_null($this->argument('user_id'))) {
            $this->getPermissionsInSelectedModel();
        } else {
            $this->getPermissionInSelectedUser();
        }

        $this->info('---');
    }


    protected function getModelsWithActiveRoles()
    {
        $models =
            $this->userHasRole->get()
                ->unique('model')
                ->pluck('model')
                ->toArray();

        $this->info('Models with active roles:');

        $this->table(['Models:'], [$models], $this->styleTable);

        $this->line('');
    }

    protected function getAllPermissions()
    {
        $permissions = app(PermissionRegistrar::class)->permissions;

        $this->info('All permissions:');

        $this->table(['Permissions:'], [$permissions], $this->styleTable);
    }

    protected function getPermissionsInSelectedModel()
    {
        $users =
            $this->userHasRole
                ->where('model', 'like', "%{$this->argument('model')}%")
                ->get();

        $model = app($users[0]['model']);

        $this->prepareDataOfUser($users, $model);
    }

    protected function getPermissionInSelectedUser()
    {
        $model_namespace =
            $this->userHasRole
                ->where('model', 'like', "%{$this->argument('model')}%")
                ->value('model');

        $user =
            app($model_namespace)->whereId($this->argument('user_id'))->get();

        $this->prepareDataOfUser($user, app($model_namespace));
    }

    protected function prepareDataOfUser($users, $model)
    {
        foreach ($users as $user) {
            $this->info('---');
            $this->info('Name: '.$model->whereId($user->id)->value($this->fieldName));
            $this->info("ID: {$user->id}");

            $permissions = $model->getPermissions($user->id);
            $permissions = str_replace('|', '', $permissions);

            $this->info("Permissions: $permissions");
        }
    }
}
