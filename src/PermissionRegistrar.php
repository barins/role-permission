<?php

namespace BorisNedovis\RolePermission;

use Illuminate\Support\Facades\Redis;
use Illuminate\Contracts\Auth\Access\Gate;
use Illuminate\Contracts\Auth\Access\Authorizable;

class PermissionRegistrar
{
    /** @var string */
    protected $roleClass;

    /** @var array */
    public $permissions;

    /** @var string */
    protected $cache_key;

    /** @var string */
    protected $redisKeyPrefix;

    /**
     * PermissionRegistrar constructor.
     */
    public function __construct()
    {
        $this->roleClass = config('role-permission.models.role');

        $this->permissions = config('role-permission.permissions');

        $this->cache_key = config('role-permission.redis.key');

        $this->redisKeyPrefix = config('role-permission.redis.prefix');
    }

    /**
     * Register the permission check method on the gate.
     * We resolve the Gate fresh here, for benefit of long-running instances.
     *
     * @return bool
     */
    public function registerPermissions(): bool
    {
        app(Gate::class)->before(function (Authorizable $user, string $ability) {
            if (method_exists($user, 'checkPermission')) {
                return $user->checkPermission($ability) ?: null;
            }
        });

        return true;
    }

    /**
     * Get all permission.
     *
     * @return array
     */
    public function getPermissions(): array
    {
        return $this->preparePermissions($this->permissions);
    }

    /**
     * Prepare permission to save
     *
     * @param $permissions array
     * @param $json_encode bool
     *
     * @return array
     */
    public function preparePermissions($permissions, $json_encode = false) {
        $result = [];

        foreach ($permissions as $permission) {
            $result[] = "|$permission|";
        }

        if($json_encode)
            $result = json_encode($permissions);

        return $result;
    }

    /**
     * Get an instance of the role class.
     *
     */
    public function getRoleClass()
    {
        return app($this->roleClass);
    }

    /**
     * Flush the cache.
     *
     * @return bool
     */
    public function forgetCachedPermissions(): bool
    {
        $keys = Redis::keys($this->cache_key. ':*');

        foreach($keys as $key) {
            Redis::del(str_replace($this->redisKeyPrefix, '', $key));
        }

        return true;
    }
}
