<?php

namespace BorisNedovis\RolePermission\Models;

use Illuminate\Database\Eloquent\Model;
use BorisNedovis\RolePermission\Exceptions\RoleAlreadyExists;
use BorisNedovis\RolePermission\Traits\RefreshesPermissionCache;

/**
 * Class UserHasRole
 *
 * @package BorisNedovis\RolePermission\Models
 *
 * @property string $model
 * @property integer $user_id
 * @property integer $role_id
 */
class UserHasRole extends Model
{
    use RefreshesPermissionCache;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'model',
        'user_id',
        'role_id'
    ];

    public function __construct()
    {
        parent::__construct();

        $this->setTable(config('role-permission.table_names.user_has_roles'));
    }

    /**
     * @param array $attributes
     *
     * @throws RoleAlreadyExists
     */
    public function save(array $attributes = [])
    {
        if (static::whereUserId($this->attributes['user_id'])
            ->whereRoleId($this->attributes['role_id'])
            ->whereModel($this->attributes['model'])
            ->first())
            throw RoleAlreadyExists::createRelation(
                $this->attributes['model'],
                $this->attributes['user_id'],
                $this->attributes['role_id']
            );

        parent::save();
    }

    public function roles()
    {
        return $this->hasMany(Role::class, 'id', 'role_id');
    }
}
