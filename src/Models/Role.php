<?php

namespace BorisNedovis\RolePermission\Models;

use BorisNedovis\RolePermission\Guard;
use Illuminate\Database\Eloquent\Model;
use BorisNedovis\RolePermission\Exceptions\RoleAlreadyExists;
use BorisNedovis\RolePermission\Traits\RefreshesPermissionCache;

/**
 * Class Role
 *
 * @package BorisNedovis\RolePermission\Models
 *
 * @property integer $id
 * @property string $name
 * @property string $permission
 * @property string $guard_name
 */
class Role extends Model
{
    use RefreshesPermissionCache;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'permission',
        'guard_name'
    ];

    /**
     * Role constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? config('auth.defaults.guard');

        parent::__construct($attributes);

        $this->setTable(config('role-permission.table_names.roles'));
    }

    /**
     * @param array $attributes
     *
     * @return mixed
     *
     * @throws  RoleAlreadyExists
     */
    public static function create(array $attributes = [])
    {
        $attributes['guard_name'] = $attributes['guard_name'] ?? Guard::getDefaultName(static::class);

        if (static::where('name', $attributes['name'])->where('guard_name', $attributes['guard_name'])->first()) {
            throw RoleAlreadyExists::create($attributes['name'], $attributes['guard_name']);
        }

        return static::query()->create($attributes);
    }
}
