<?php

namespace BorisNedovis\RolePermission\Traits;

use BorisNedovis\RolePermission\Models\Role as RoleModel;
use BorisNedovis\RolePermission\Models\UserHasRole;

/**
 * Trait Role
 *
 * @package BorisNedovis\RolePermission\Traits
 */
trait Role
{
    /**
     * @param RoleModel $role
     */
    public function addRole(RoleModel $role): void
    {
        $relationRoleAndUser = New UserHasRole();

        $relationRoleAndUser->model = static::class;
        $relationRoleAndUser->user_id = $this->id;
        $relationRoleAndUser->role_id = $role->id;

        $relationRoleAndUser->save();
    }

    /**
     * @param RoleModel $role
     */
    public function deleteRole(RoleModel $role): void
    {
        UserHasRole::whereUserId($this->id)
            ->whereRoleId($role->id)
            ->first()
            ->delete();
    }
}