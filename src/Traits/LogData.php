<?php
namespace BorisNedovis\RolePermission\Traits;

use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Auth;
/**
 * Trait LogData
 *
 * For custom log data
 *
 * @package App\Traits
 *
 * @property bool   $logRecord   required
 * @property string $logChannel  required
 * @property string $logShowAuth required
 */
trait LogData
{
    /**
     * @param $log
     */
    public function log($log)
    {
        if($this->logRecord) {

            $logManager = Log::channel($this->logChannel);

            if($this->logShowAuth) {
                $guard = Auth::getDefaultDriver();
                $logManager->info($log . " | Guard: $guard; Id:". Auth::guard($guard)->id());
            } else {
                $logManager->info($log);
            }
        }
    }
}
