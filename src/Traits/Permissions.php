<?php

namespace BorisNedovis\RolePermission\Traits;

use Illuminate\Support\Facades\Redis;
use BorisNedovis\RolePermission\Models\Role;
use BorisNedovis\RolePermission\Models\UserHasRole;

/**
 * Trait Permissions
 *
 * @package BorisNedovis\RolePermission\Traits
 */
trait Permissions
{
    /**
     * Main check access.
     *
     * @param string $permission
     * @param null|integer $user_id
     *
     * @return  bool
     */
    public function checkPermission($permission, $user_id = null): bool
    {
        if(is_null($user_id))
            $user_id = $this->id;

        $full_cache_key = config('role-permission.redis.key').':'.static::class.':'.$user_id;

        $user_permission = Redis::get($full_cache_key);

        if(is_null($user_permission)) {
            $user_permission = self::getPermissions($user_id);
            self::setPermissions($user_permission, $user_id);
        }

        return
            strpos($user_permission, "|$permission|") !== false;
    }

    /**
     * Set access to a specific user.
     *
     * @param  string  $permissions
     * @param  integer  $user_id
     */
    public static function setPermissions($permissions, $user_id): void
    {
        $full_cache_key = config('role-permission.redis.key').':'.static::class.':'.$user_id;

        Redis::set($full_cache_key, $permissions);
    }

    /**
     * @param $user_id
     *
     * @return array|string
     */
    public static function getPermissions($user_id): string
    {
        // Getting all role in current user
        $role_ids = UserHasRole::whereModel(static::class)
            ->where('user_id', $user_id)
            ->pluck('role_id');

        $permissions = Role::whereIn('id', $role_ids)->pluck('permission');

        $result = [];

        foreach ($permissions as $permission) {

            $preparePermissions = json_decode($permission);

            foreach ($preparePermissions as $title) {

                if(! in_array($title, $result))
                    $result[] = "|$title|";

            }

        }

        $result = json_encode($result);

        return $result;
    }
}
