<?php

namespace BorisNedovis\RolePermission\Traits;

use BorisNedovis\RolePermission\PermissionRegistrar;

/**
 * Trait RefreshesPermissionCache
 *
 * @package BorisNedovis\RolePermission\Traits
 */
trait RefreshesPermissionCache
{
    public static function bootRefreshesPermissionCache()
    {
        static::saved(function () {
            app(PermissionRegistrar::class)->forgetCachedPermissions();
        });

        static::deleted(function () {
            app(PermissionRegistrar::class)->forgetCachedPermissions();
        });
    }
}
